# AsiLoaderDE

Asi-loader for **GTA III/VC/SA Definitive Edition**



## Insatllation

### AsiLoader

Copy files **libvorbisfile_64.dll** and **hklibvorbisfile_64.dll** to **GAME FOLDER -> Engine -> Binaries -> ThirdParty -> Vorbis -> Win64 -> VS2015**

### Asi plugins

Copy **\*.asi** files to **GAME FOLDER -> Engine -> Binaries -> ThirdParty -> Vorbis -> Win64 -> VS2015**



## [Download](https://gitlab.com/prime-hack/samp/plugins/AsiLoaderDE/-/jobs/artifacts/main/download?job=win64)
