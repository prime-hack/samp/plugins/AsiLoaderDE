#pragma once

#include <cstdint>

typedef struct {
	size_t ( *read_func )( void *ptr, size_t size, size_t nmemb, void *datasource );
	int ( *seek_func )( void *datasource, std::int64_t offset, int whence );
	int ( *close_func )( void *datasource );
	long ( *tell_func )( void *datasource );
} ov_callbacks;

extern "C" __declspec( dllexport ) int ov_clear( void *vf );
extern "C" __declspec( dllexport ) int ov_fopen( const char *path, void *vf );
extern "C" __declspec( dllexport ) int ov_open( void *f, void *vf, const char *initial, long ibytes );
extern "C" __declspec(
	dllexport ) int ov_open_callbacks( void *datasource, void *vf, const char *initial, long ibytes, ov_callbacks callbacks );
extern "C" __declspec( dllexport ) int ov_test( void *f, void *vf, const char *initial, long ibytes );
extern "C" __declspec(
	dllexport ) int ov_test_callbacks( void *datasource, void *vf, const char *initial, long ibytes, ov_callbacks callbacks );
extern "C" __declspec( dllexport ) int ov_test_open( void *vf );
extern "C" __declspec( dllexport ) long ov_bitrate( void *vf, int i );
extern "C" __declspec( dllexport ) long ov_bitrate_instant( void *vf );
extern "C" __declspec( dllexport ) long ov_streams( void *vf );
extern "C" __declspec( dllexport ) long ov_seekable( void *vf );
extern "C" __declspec( dllexport ) long ov_serialnumber( void *vf, int i );
extern "C" __declspec( dllexport ) std::int64_t ov_raw_total( void *vf, int i );
extern "C" __declspec( dllexport ) std::int64_t ov_pcm_total( void *vf, int i );
extern "C" __declspec( dllexport ) double ov_time_total( void *vf, int i );
extern "C" __declspec( dllexport ) int ov_raw_seek( void *vf, std::int64_t pos );
extern "C" __declspec( dllexport ) int ov_pcm_seek( void *vf, std::int64_t pos );
extern "C" __declspec( dllexport ) int ov_pcm_seek_page( void *vf, std::int64_t pos );
extern "C" __declspec( dllexport ) int ov_time_seek( void *vf, double pos );
extern "C" __declspec( dllexport ) int ov_time_seek_page( void *vf, double pos );
extern "C" __declspec( dllexport ) int ov_raw_seek_lap( void *vf, std::int64_t pos );
extern "C" __declspec( dllexport ) int ov_pcm_seek_lap( void *vf, std::int64_t pos );
extern "C" __declspec( dllexport ) int ov_pcm_seek_page_lap( void *vf, std::int64_t pos );
extern "C" __declspec( dllexport ) int ov_time_seek_lap( void *vf, double pos );
extern "C" __declspec( dllexport ) int ov_time_seek_page_lap( void *vf, double pos );
extern "C" __declspec( dllexport ) std::int64_t ov_raw_tell( void *vf );
extern "C" __declspec( dllexport ) std::int64_t ov_pcm_tell( void *vf );
extern "C" __declspec( dllexport ) double ov_time_tell( void *vf );
extern "C" __declspec( dllexport ) void *ov_info( void *vf, int link );
extern "C" __declspec( dllexport ) void *ov_comment( void *vf, int link );
extern "C" __declspec( dllexport ) long ov_read_float( void *vf, float ***pcm_channels, int samples, int *bitstream );
extern "C" __declspec( dllexport ) long ov_read_filter( void *vf,
														char *buffer,
														int length,
														int bigendianp,
														int word,
														int sgned,
														int *bitstream,
														void ( *filter )( float **pcm, long channels, long samples, void *filter_param ),
														void *filter_param );
extern "C" __declspec( dllexport ) long ov_read( void *vf, char *buffer, int length, int bigendianp, int word, int sgned, int *bitstream );
extern "C" __declspec( dllexport ) int ov_crosslap( void *vf1, void *vf2 );
extern "C" __declspec( dllexport ) int ov_halfrate( void *vf, int flag );
extern "C" __declspec( dllexport ) int ov_halfrate_p( void *vf );
