#include "library.h"

#include <filesystem>
#include <fstream>
#include <vector>

#include "mem/mem.h"

#include <windows.h>

EXTERN_C IMAGE_DOS_HEADER __ImageBase;
std::ofstream logFile;
HMODULE origLib = nullptr;

[[maybe_unused]] class Loader {
	std::vector<HMODULE> asiPlugins_;

public:
	Loader() noexcept {
		auto dir = getLibDirectory();

		logFile.open( dir / "AsiLoader.log" );

		origLib = LoadLibraryW( ( dir / "hklibvorbisfile_64.dll" ).c_str() );
		if ( !origLib || origLib == INVALID_HANDLE_VALUE ) {
			if ( logFile.is_open() )
				logFile << "Can't find original library in " << dir << std::endl;
			else {
				auto err = L"Can't find load library from " + ( dir / "hklibvorbisfile_64.dll" ).wstring();
				MessageBoxW( nullptr, err.c_str(), L"AsiLoader: can't load original", 0 );
			}
			return;
		}

		loadAsiPlugins( dir );
		loadAsiPlugins();
		std::reverse( asiPlugins_.begin(), asiPlugins_.end() );

		if ( logFile.is_open() ) logFile << "Load complete" << std::endl;
	}
	~Loader() {
		for ( auto &&hLib : asiPlugins_ ) FreeLibrary( hLib );
		if ( logFile.is_open() ) {
			logFile << "Unload complete" << std::endl;
			logFile.close();
		}
	}

protected:
	static std::filesystem::path getLibDirectory() {
		wchar_t buf[0x400]{ 0 };
		GetModuleFileNameW( (HMODULE)&__ImageBase, buf, sizeof( buf ) / sizeof( wchar_t ) );
		return std::filesystem::path( buf ).remove_filename();
	}

	void loadAsiPlugins( const std::filesystem::path &dir = std::filesystem::current_path() ) {
		if ( logFile.is_open() ) logFile << "Load asi-plugins from directory " << dir << std::endl;
		for ( auto &&entry : std::filesystem::directory_iterator( dir ) ) {
			if ( entry.is_directory() ) continue;
			if ( entry.path().extension() != ".asi" ) continue;
			auto hLib = LoadLibraryW( entry.path().c_str() );
			auto err = GetLastError();
			if ( logFile.is_open() ) {
				logFile << "Load ASI " << entry.path().filename();
				if ( hLib && hLib != INVALID_HANDLE_VALUE ) {
					logFile << " at " << std::hex << hLib << std::endl;
					asiPlugins_.push_back( hLib );
				} else
					logFile << " failed " << std::hex << err << std::endl;
			}
		}
	}
} g_loader;

int ov_clear( void *vf ) {
	static auto fn = (int( * )( void *vf ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf );
}
int ov_fopen( const char *path, void *vf ) {
	static auto fn = (int( * )( const char *path, void *vf ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( path, vf );
}
int ov_open( void *f, void *vf, const char *initial, long ibytes ) {
	static auto fn = (int( * )( void *f, void *vf, const char *initial, long ibytes ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( f, vf, initial, ibytes );
}
int ov_open_callbacks( void *datasource, void *vf, const char *initial, long ibytes, ov_callbacks callbacks ) {
	static auto fn =
		(int( * )( void *datasource, void *vf, const char *initial, long ibytes, ov_callbacks callbacks ))GetProcAddress( origLib,
																														  __FUNCTION__ );
	return fn( datasource, vf, initial, ibytes, callbacks );
}
int ov_test( void *f, void *vf, const char *initial, long ibytes ) {
	static auto fn = (int( * )( void *f, void *vf, const char *initial, long ibytes ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( f, vf, initial, ibytes );
}
int ov_test_callbacks( void *datasource, void *vf, const char *initial, long ibytes, ov_callbacks callbacks ) {
	static auto fn =
		(int( * )( void *datasource, void *vf, const char *initial, long ibytes, ov_callbacks callbacks ))GetProcAddress( origLib,
																														  __FUNCTION__ );
	return fn( datasource, vf, initial, ibytes, callbacks );
}
int ov_test_open( void *vf ) {
	static auto fn = (int( * )( void *vf ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf );
}
long ov_bitrate( void *vf, int i ) {
	static auto fn = (long( * )( void *vf, int i ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, i );
}
long ov_bitrate_instant( void *vf ) {
	static auto fn = (long( * )( void *vf ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf );
}
long ov_streams( void *vf ) {
	static auto fn = (long( * )( void *vf ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf );
}
long ov_seekable( void *vf ) {
	static auto fn = (long( * )( void *vf ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf );
}
long ov_serialnumber( void *vf, int i ) {
	static auto fn = (long( * )( void *vf, int i ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, i );
}
std::int64_t ov_raw_total( void *vf, int i ) {
	static auto fn = (std::int64_t( * )( void *vf, int i ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, i );
}
std::int64_t ov_pcm_total( void *vf, int i ) {
	static auto fn = (std::int64_t( * )( void *vf, int i ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, i );
}
double ov_time_total( void *vf, int i ) {
	static auto fn = (double( * )( void *vf, int i ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, i );
}
int ov_raw_seek( void *vf, std::int64_t pos ) {
	static auto fn = (int( * )( void *vf, std::int64_t pos ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, pos );
}
int ov_pcm_seek( void *vf, std::int64_t pos ) {
	static auto fn = (int( * )( void *vf, std::int64_t pos ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, pos );
}
int ov_pcm_seek_page( void *vf, std::int64_t pos ) {
	static auto fn = (int( * )( void *vf, std::int64_t pos ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, pos );
}
int ov_time_seek( void *vf, double pos ) {
	static auto fn = (int( * )( void *vf, double pos ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, pos );
}
int ov_time_seek_page( void *vf, double pos ) {
	static auto fn = (int( * )( void *vf, double pos ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, pos );
}
int ov_raw_seek_lap( void *vf, std::int64_t pos ) {
	static auto fn = (int( * )( void *vf, std::int64_t pos ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, pos );
}
int ov_pcm_seek_lap( void *vf, std::int64_t pos ) {
	static auto fn = (int( * )( void *vf, std::int64_t pos ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, pos );
}
int ov_pcm_seek_page_lap( void *vf, std::int64_t pos ) {
	static auto fn = (int( * )( void *vf, std::int64_t pos ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, pos );
}
int ov_time_seek_lap( void *vf, double pos ) {
	static auto fn = (int( * )( void *vf, double pos ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, pos );
}
int ov_time_seek_page_lap( void *vf, double pos ) {
	static auto fn = (int( * )( void *vf, double pos ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, pos );
}
std::int64_t ov_raw_tell( void *vf ) {
	static auto fn = (std::int64_t( * )( void *vf ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf );
}
std::int64_t ov_pcm_tell( void *vf ) {
	static auto fn = (std::int64_t( * )( void *vf ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf );
}
double ov_time_tell( void *vf ) {
	static auto fn = (double( * )( void *vf ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf );
}
void *ov_info( void *vf, int link ) {
	static auto fn = (void *(*)( void *vf, int link ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, link );
}
void *ov_comment( void *vf, int link ) {
	static auto fn = (void *(*)( void *vf, int link ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, link );
}
long ov_read_float( void *vf, float ***pcm_channels, int samples, int *bitstream ) {
	static auto fn = (long( * )( void *vf, float ***pcm_channels, int samples, int *bitstream ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, pcm_channels, samples, bitstream );
}
long ov_read_filter( void *vf,
					 char *buffer,
					 int length,
					 int bigendianp,
					 int word,
					 int sgned,
					 int *bitstream,
					 void ( *filter )( float **pcm, long channels, long samples, void *filter_param ),
					 void *filter_param ) {
	static auto fn = (long( * )( void *vf,
								 char *buffer,
								 int length,
								 int bigendianp,
								 int word,
								 int sgned,
								 int *bitstream,
								 void ( *filter )( float **pcm, long channels, long samples, void *filter_param ),
								 void *filter_param ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, buffer, length, bigendianp, word, sgned, bitstream, filter, filter_param );
}
long ov_read( void *vf, char *buffer, int length, int bigendianp, int word, int sgned, int *bitstream ) {
	static auto fn = (long( * )( void *vf, char *buffer, int length, int bigendianp, int word, int sgned, int *bitstream ))GetProcAddress(
		origLib,
		__FUNCTION__ );
	return fn( vf, buffer, length, bigendianp, word, sgned, bitstream );
}
int ov_crosslap( void *vf1, void *vf2 ) {
	static auto fn = (int( * )( void *vf1, void *vf2 ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf1, vf2 );
}
int ov_halfrate( void *vf, int flag ) {
	static auto fn = (int( * )( void *vf, int flag ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf, flag );
}
int ov_halfrate_p( void *vf ) {
	static auto fn = (int( * )( void *vf ))GetProcAddress( origLib, __FUNCTION__ );
	return fn( vf );
}
